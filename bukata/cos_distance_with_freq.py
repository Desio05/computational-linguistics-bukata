# classwork 3 and homework 3

import operator

from pymystem3 import Mystem
from scipy import spatial

from bukata.inverted_index_search import *


def get_word_freq_from_doc_and_query(query, inverted_index_with_freq):
    """
    Computes dictionary with documents as keys and words (and its frequencies) as values
    """
    docs = set()
    docs_for_query = []
    for word in query:
        docs_for_query.append(inverted_index_with_freq[word])
        docs = docs.union(set(inverted_index_with_freq[word].keys()))

    docs_dict = {}
    for doc in docs:
        current = {}
        for index, word in enumerate(query):
            if doc in docs_for_query[index]:
                current[word] = docs_for_query[index][doc]
        docs_dict[doc] = current
    return docs_dict


def get_tf_vectors_from_doc_word_freq(query, docs_dict):
    """
    Computes term frequencies from query and dictionary with documents as keys and words (and its frequencies) as values
    :return dictionary with documents as keys and array of word TFs (in order of queried)
    """
    tf_vectors = {}
    for doc in docs_dict.keys():
        max_freq = max([freq for freq in docs_dict[doc].values()])
        tf_vectors[doc] = []
        for word in query:
            if word in docs_dict[doc]:
                tf_vectors[doc].append(docs_dict[doc][word] / max_freq)
            else:
                tf_vectors[doc].append(0)
    return tf_vectors


def get_query_tfs(query):
    """
    Computes term frequencies for query
    :return array of query word TFs (in order of queried)
    """
    word_freq = {word: query.count(word) for word in query}
    query_doc_dict = {0: word_freq}
    return get_tf_vectors_from_doc_word_freq(query, query_doc_dict)[0]


if __name__ == '__main__':
    space_inverted_index_with_freq = build_index_with_freq_for_xml("resources/space.xml")
    philosophy_inverted_index_with_freq = build_index_with_freq_for_xml("resources/philosophy.xml")

    query_text = "специальная теория нобелевских лауреатов о скорости звезды в космосе"
    stem = Mystem()
    my_stopwords = stopwords + [' ', ',', '\n']
    query_words = [candidate for candidate in stem.lemmatize(query_text) if candidate not in my_stopwords]

    doc_words_freq = get_word_freq_from_doc_and_query(query_words, space_inverted_index_with_freq)
    doc_word_tfs = get_tf_vectors_from_doc_word_freq(query_words, doc_words_freq)
    query_word_tfs = get_query_tfs(query_words)

    cos_distances = {}
    for doc in doc_word_tfs.keys():
        cos_distances[doc] = spatial.distance.cosine(doc_word_tfs[doc], query_word_tfs)

    # pprint(cos_distances)
    print(dict(sorted(cos_distances.items(), key=operator.itemgetter(1), reverse=True)[:5]))
