# classwork 4 and homework 4
from math import log

from bukata.cos_distance_with_freq import *
from bukata.inverted_index_search import *


def update_birm_relevance(birm_relevance, search_words, inverted_index_with_freq, relevant_docs, sentences_number):
    for word in search_words:
        if not inverted_index_with_freq.get(word):
            pass

        n = set(inverted_index_with_freq.get(word).keys())  # docs containing term
        r = n.intersection(relevant_docs)  # relevant docs containing term
        r_all = relevant_docs  # relevant docs
        n_all = sentences_number

        # new weight for term
        weight = log(((len(r) + 0.5) / (len(r_all) - len(r) + 0.5)) /
                     ((len(n) - len(r) + 0.5) / (n_all - len(n) - len(r_all) + len(r) + 0.5)))
        for doc in n:
            inverted_index_with_freq.get(word)[doc] = weight


if __name__ == '__main__':
    space_inverted_index_with_freq = build_index_with_freq_for_xml("resources/space.xml")
    philosophy_inverted_index_with_freq = build_index_with_freq_for_xml("resources/philosophy.xml")

    query_text = "специальная теория нобелевских лауреатов о скорости звезды в космосе"
    stem = Mystem()
    my_stopwords = stopwords + [' ', ',', '\n']
    query_words = [candidate for candidate in stem.lemmatize(query_text) if candidate not in my_stopwords]

    doc_words_freq = get_word_freq_from_doc_and_query(query_words, space_inverted_index_with_freq)

    # BIRM without feedback
    birm_relevance = {}
    for doc in doc_words_freq.keys():
        birm_relevance[doc] = sum(doc_words_freq[doc].values())
    print("BIRM without feedback:")
    print(dict(sorted(birm_relevance.items(), key=operator.itemgetter(1), reverse=True)[:5]))

    # BIRM with feedback
    xml_parser = etree.XMLParser(ns_clean=True, remove_blank_text=True)
    tree = etree.parse("resources/space.xml", parser=xml_parser)
    sentences_amount = len(tree.getroot())

    relevant_docs = input("Type relevant docs:\n")
    user_relevance_docs = [int(doc) for doc in relevant_docs.strip().split(" ")]
    update_birm_relevance(birm_relevance, query_words, space_inverted_index_with_freq, user_relevance_docs,
                          sentences_amount)
    # recalculate birm
    doc_words_freq = get_word_freq_from_doc_and_query(query_words, space_inverted_index_with_freq)
    birm_relevance = {}
    for doc in doc_words_freq.keys():
        birm_relevance[doc] = sum(doc_words_freq[doc].values())

    print("BIRM with feedback:")
    print(dict(sorted(birm_relevance.items(), key=operator.itemgetter(1), reverse=True)[:5]))
