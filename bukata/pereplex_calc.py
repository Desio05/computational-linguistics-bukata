# classwork 6 and homework 6
import codecs
import os
import pickle
from math import *

import pymorphy2
from lxml import etree


def process_nkrya_raw_unigrams():
    unigram_pickle_path = "resources/nkrya/backups/unigram.pckl"
    if os.path.isfile(unigram_pickle_path):
        return pickle.load(open(unigram_pickle_path, "rb"))

    unigrams = codecs.open("resources/nkrya/1grams.txt", "r", "utf_8").read()
    morph = pymorphy2.MorphAnalyzer()
    result = {}
    unigrams_split = unigrams.split('\n')
    i = 0
    for item in unigrams_split:
        freq_word = item.split('\t')
        if len(freq_word) < 2:
            continue
        freq, word = freq_word
        freq = int(freq)
        word_normal_form = morph.parse(word)[0].normal_form
        if word_normal_form in result:
            result[word_normal_form] += freq
        else:
            result[word_normal_form] = freq
        i += 1
        if i % 1000 == 0:
            print(int((len(unigrams_split) - i) / 1000))

    with open(unigram_pickle_path, "wb") as output:
        pickle.dump(result, output)

    return result


def process_nkrya_raw_bigrams():
    unigram_pickle_path = "resources/nkrya/backups/bigram.pckl"
    if os.path.isfile(unigram_pickle_path):
        return pickle.load(open(unigram_pickle_path, "rb"))

    unigrams = codecs.open("resources/nkrya/2grams.txt", "r", "utf_8").read()
    morph = pymorphy2.MorphAnalyzer()
    result = {}
    unigrams_split = unigrams.split('\n')
    i = 0
    for item in unigrams_split:
        freq_word = item.split('\t')
        if len(freq_word) < 2:
            continue
        freq, word1, _, word2 = freq_word
        freq = int(freq)
        word1_normal_form = morph.parse(word1)[0].normal_form
        word2_normal_form = morph.parse(word2)[0].normal_form
        if (word1_normal_form, word2_normal_form) in result:
            result[(word1_normal_form, word2_normal_form)] += freq
        else:
            result[(word1_normal_form, word2_normal_form)] = freq
        i += 1
        if i % 1000 == 0:
            print(int((len(unigrams_split) - i) / 1000))

    with open(unigram_pickle_path, "wb") as output:
        pickle.dump(result, output)

    return result


def calculate_unigram_probability():
    unigram = process_nkrya_raw_unigrams()
    train_word_count = sum(unigram.values())
    result = {}
    for item in unigram.keys():
        result[item] = log(unigram[item] / float(train_word_count))
    return result


def calculate_bigram_probability():
    unigram = process_nkrya_raw_unigrams()
    bigram = process_nkrya_raw_bigrams()
    result = {}
    for word1, word2 in bigram.keys():
        result[(word1, word2)] = log(bigram[(word1, word2)] / unigram[word1])
    return result


def calculate_pereplex_unigram(path_to_xml):
    xml_parser = etree.XMLParser(ns_clean=True, remove_blank_text=True)
    tree = etree.parse(path_to_xml, parser=xml_parser)
    morph = pymorphy2.MorphAnalyzer()
    nkrya_words_probability = calculate_unigram_probability()
    sentences_probabilities_sum = 0
    for sentence in tree.getroot():
        normal_words = [morph.parse(word)[0].normal_form for word in sentence.text.split(' ')]
        sentence_probability = 0
        for word in normal_words:
            if word not in nkrya_words_probability:
                sentence_probability = 0
                break
            # by mistake, every word probability was calculated with log_e
            sentence_probability += exp(nkrya_words_probability[word])
        # TODO: what should we do if sentence_probability is 0?
        # TODO: (one of the words from the sentence has 0 probability to be generated with nkrya train data)
        sentences_probabilities_sum += log(sentence_probability) if sentence_probability > 0 else 0
    perplexity = 2 ** (- sentences_probabilities_sum / len(tree.getroot()))
    return perplexity


def calculate_pereplex_bigram(path_to_xml):
    xml_parser = etree.XMLParser(ns_clean=True, remove_blank_text=True)
    tree = etree.parse(path_to_xml, parser=xml_parser)
    morph = pymorphy2.MorphAnalyzer()
    nkrya_words_probability = calculate_bigram_probability()
    sentences_probabilities_sum = 0
    for sentence in tree.getroot():
        words = [morph.parse(word)[0].normal_form for word in sentence.text.split(' ')]
        sentence_probability = 0
        for i in range(len(words) - 1):
            if (words[i], words[i + 1]) not in nkrya_words_probability:
                sentence_probability = 0
                break
            # by mistake, every word probability was calculated with log_e
            sentence_probability += exp(nkrya_words_probability[(words[i], words[i + 1])])
        # TODO: what should we do if sentence_probability is 0?
        # TODO: (one of the words from the sentence has 0 probability to be generated with nkrya train data)
        sentences_probabilities_sum += log(sentence_probability) if sentence_probability > 0 else -inf
    perplexity = 2 ** (- sentences_probabilities_sum / len(tree.getroot()))
    return perplexity


# print(calculate_pereplex_unigram("resources/space.xml"))
print(calculate_pereplex_bigram("resources/philosophy.xml"))
