Если вы хоть немного похожи на нас, то выросли с убеждением, что вы — центр Вселенной, и на первый взгляд наблюдения Хаббла подтверждают эту теорию. Кажется, что все галактики разбегаются от нас врассыпную (хорошо, если хотите, Вселенная расширяется вокруг нас), и напрашивается мысль, что мы выгодно отличаемся от иных прочих. Ведь если все галактики удаляются от нас, значит, мы находимся в центре, как же иначе?

Позвольте представить: тентакуляне, раса астрономов из галактики, которая находится в миллиарде световых лет от нашей. Один из величайших астрономов — создание по имени доктор Калачик. Хотите познакомиться с доктором Калачиком? К сожалению, вынуждены вас огорчить. Поскольку эта галактика находится в миллиарде светоёых лет от нашей, то даже если бы мы послали на Тентаку- люс VII радиограмму и попросили доктора Калачика ответить, у самого доктора Калачика это вряд ли получилось бы из-за крайне преклонного возраста. Если бы вам очень повезло, вы бы получили ответ от его прапрапра (умножьте миллионов на 50) правнучки, и к тому времени, как она ответила бы вам и указала на ошибку, пройдет еще добрый милли-
Вселенная: руководство по эксплуатации

ард лет (даже если она не станет тянуть с ответом, а тогда наши потомки для начала просто забудут, что мы пытались с кем-то там наладить радиосвязь. Так что лично познакомиться с доктором Калачиком мы не можем, поэтому не можем и спросить у него, что же он видит в телескоп.

На самом деле все еще сложнее — ведь Вселенная расширяется. Если мы отправим сигнал на Тентакулюс VII, он будет идти туда больше миллиарда лет — и еще дольше будет идти ответ. Это все равно что пытаться измерить угря. Пока берешь линейку, он извивается со страшной силой, а пока приложишь к линейке голову, понимаешь, что нулевая отметка совсем не там, где ты ее оставил.

Это ничего, мы все равно знаем, что видит доктор Калачик в телескоп. Он видит в точности то же самое, что и мы с Земли,— почти все галактики в небесах разлетаются от Тентакулюса VII, и чем дальше они находятся, тем быстрее, как представляется, летят. Шовинистически настроенные элементы на Тентакулюсе уже постановили интерпретировать эти наблюдения как несокрушимое доказательство того, что Тентакулюс — центр Вселенной.

Разве может быть так, что прав и доктор Хаббл, и доктор Калачик? Разве может быть так, что обе галактики находятся в центре Вселенной?

Представьте себе, что вы жарите оладьи с черникой. Эту добавку мы выбрали по двум причинам: во-первых, любим чернику, во-вторых, черничины, как и галактики, в процессе приготовления оладий сами по себе не расширяются. Когда тесто для оладий поднимается и расширяется, черничины начинают удаляться друг от друга. Если бы они были разумны, все и каждая из них думали бы одно и то же: все прочие черничины разбегаются от меня, а дальние движутся быстрее, чем ближние!»[85]

Это подводит нас к достаточно деликатному вопросу, который вам, вероятно, покажется знакомым, если вы вспомните главу 1. Если у всех во Вселенной складывается впечатление, что все остальные от них разлетаются, разве можно утверждать, будто кто-то вообще двигается?

По всей истории науки красной нитью проходит общая тема «антицентропупизма». Николай Коперник (в честь которого назван «принцип Коперника») доказал, что Земля —не центр Солнечной системы. В 1918 году Харлоу Шапли из Гарварда показал, что наша Солнечная система находится на глухой периферии галактики Млечный Путь — во-
Вселенная: руководство по эксплуатации

преки распространенному заблуждению. А теперь Хаббл (и доктор Калачик на своей планете) утверждают, что наша Галактика — вовсе не центр Вселенной!

Однако» как мы сказали, с уверенностью назвать себя центром Вселенной не вправе никто. Предлагаем вам аналогию: представьте себе, что вы муравей, живущий на поверхности воздушного шара. Когда шар надувается, вы видите, что все остальные муравьи разлетаются от вас все дальше и дальше.

Заядлый зануда нашел бы возражения против мира муравьев. Он бы сказал: «Минуточку! Я знаю, что если бы мир муравьев надувался, муравьи бы заметили! Ведь я же замечаю, когда моя мама, скажем, нажимает на педаль газа!» Да, это так, но в нашем случае муравьи ничего не замечают, потому что их вселенная расширяется в загадочном третьем измерении, которого они прямо не воспринимают[86].

Вероятно, мы движемся в четвертом пространственном измерении, которое отличается от привычной системы координат из трех пространственных осей и оси времени. Чуть ниже мы поговорим о том, возможно ли, что существуют и другие измерения, кроме трех, которые мы воспринимаем непосредственно. Вероятно, это как раз тот случай, когда аналогия заводит слишком далеко. По принятой сейчас стандартной космологической модели, мы не нуждаемся ни в каких измерениях, кроме трех известных (плюс время).

Разговор о Тентакулюсе VII наталкивает нас на важные размышления. Если бы у нас были такие мощные телескопы, что в них можно было бы разглядеть родную планету доктора Калачика, мы бы увидели не то, что там происходит сегодня, а то, что было примерно миллиард лет назад. А если бы мы поглядели на другую, еще более отдаленную галактику, то заглянули бы в еще более отдаленное прошлое. Именно так ученые и изучают ранние стадии развития Вселенной — они смотрят, что происходит в очень далеких галактиках.

Однако за самыми дальними галактиками существует предел, за который мы заглянуть не в силах. На Земле мы называем этот предел горизонтом, но точно такой же горизонт существует и у Вселенной в целом. Заглянуть за горизонт мы не можем, так как свет распространяется с постоянной скоростью.

А поскольку Вселенная существует относительно недавно, всего каких-то 13,7 миллиарда лет, все, что расположено дальше, чем 13,7 миллиарда световых лет, еще некоторое время не будет доступно нашему глазу.

А откуда, собственно, взялась эта дата «начала Вселенной»? Начнем с конца. Если все галактики во Вселенной удаляются друг от друга, значит, когда-то в прошлом был момент, когда они (или по - крайней мере атомы, которые их составляют) сидели друг у друга на голове. Это «событие» мы называем Большим взрывом, который стал причиной крупных заблуждений, всяческой путаницы и написания следующей главы.

Оценить, когда произошел Большой взрыв, мы сумеем, если вспомним, что скорость — это отношение расстояния ко времени. Предположив (ошибочно, как выясняется, но пока что такая погрешность нас устраивает), будто скорость удаления галактики, где расположен Тентакулюс, с начала времен постоянна, мы можем вычислить скорость Вселенной при помощи простых магоматематических выкладок. Только подумайте: чем дальше от нас галактика находится сегодня, тем старше наша Вселенная, поскольку все разбегается друг от друга в известном нам темпе. Подставим в это простенькое линейное уравнение переменные, справедливые для нашей Вселенной, и прикинем, что возраст Вселенной — около 13,8 миллиарда лет: смотрите, результат почти такой же, как если бы вы проделали все вычисления точно и с нужными поправками. I Если бы у нас был достаточно мощный телескоп, смогли бы мы своими глазами увидеть зарождение Вселенной? Почти, но не совсем. Нынешний рекордсмен по дальности, объект по прозвищу А1689-zD1, находится от нас на таком расстоянии, что его изображение, видное в космический телескоп «Хаббл», относится к тому времени, когда Вселенная насчитывала всего 700 миллионов лет от роду (около 5% ее нынешнего возраста), когда ее размер составлял меньше 1/8 нынешнего.

Хуже того, А 1689-zD1 удаляется от нас со скоростью, примерно в 8 раз превышающей скорость света. (Мы подождем, а вы перелистайте книжку назад, на главу 1, где мы четко и недвусмысленно заявили, что это невозможно.) Загадка мгновенно разрешится, если мы вспомним, что это Вселенная расширяется, а не галактика движется. Галактика стоит на месте.

Вам все еще кажется, что мы жульничаем? Вовсе нет. Специальная теория относительности не говорит, что предметы не могут удаляться друг от друга со скоростью больше скорости света. А говорит она следующее: если.я отправлю в небо Бэт-сигнал, Бэт- мен не сумеет перегнать его на Бэтплане, как бы ни пыжился. В более общем смысле это означает, что никакая информация (например, частица или сигнал) не может двигаться быстрее света. Это абсолютная правда, даже если Вселенная очень быстро расширяется. Мы не в состоянии использовать расширение Вселенной, чтобы обогнать луч света.

На самом деле мы способны заглянуть в прошлое даже дальше, чем А1689-zD1, но для этого нам нужны радиоприемники. Мы можем заглянуть в то время, когда Вселенной было всего-навсего 380 тысяч лет от роду и она состояла всего лишь из бурлящей смеси водорода, гелия и крайне высокоэнергичного излучения.

Дальше все в тумане — буквально. Поскольку Вселенная на ранних стадиях своего развития была туго набита материей, это все равно что пытаться заглянуть за соседкины шторы[87]. Что за ними, не видно, но мы знаем, как выглядит Вселенная сейчас и как она выглядела в каждый момент времени с ранних стадий до сегодняшнего дня, поэтому можем догадаться, что находится за этой космической шторой. Так и подмывает за нее заглянуть, правда?

Так вот, хотя заглянуть за горизонт мы не в силах, зато видим достаточно много, чтобы удовлетворять собственное и чужое любопытство за государственный счет. Самое прекрасное — чем дольше мы ждем» тем старше становится Вселенная и тем дальше отодвигается горизонт. Иначе говоря, существуют далекие уголки Вселенной, чей свет доходит до нас только сейчас.

А что же находится за горизонтом? Этого никто не знает, но мы вправе делать обоснованные догадки. Помните, что Коперник и его последователи ясно показали нам; «Когда куда-нибудь идешь, то все равно куда-нибудь придешь», поэтому можно предположить, что за горизонтом Вселенная выглядит примерно так же, как и здесь. Конечно, там будут другие галактики, но их окажется примерно столько же, что и вокруг нас, и выглядеть они будут примерно так же, как и наши соседки. Но это не обязательно правда. Мы выдвигаем такое предположение, поскольку у нас нет причин думать иначе.

Так, значит, Вселенная расширяется, однако галактики в ней практически не движутся. Как же
Вселенная: руководство по эксплуатации

это все на самом деле устроено? Придется вернуться к эйнштейновской общей теории относительности. Джон Арчибальд Уилер блестяще описал эту теорию известным афоризмом: «Пространство диктует материи, как двигаться, а материя диктует пространству, как искривляться», и именно так и следует о ней думать.

Мы не забыли о своем обещании держаться подальше от математики, однако формулировка Уилера, по сути, — это сухое изложение главного уравнения общей теории относительности — эйнштейновского уравнения поля. Приводить его здесь мы не будем, но кое-что о нем нужно знать.

Левая сторона уравнения поля[88] определяет, насколько две точки далеки друг от друга и в пространстве, и во времени,— эта величина называется «метрика»,— а если мы посмотрим, как метрика меняется в пространстве, то сможем описать, насколько оно искривлено. Метрике отводится настолько важная роль, поскольку частицы ленивы и выбирают именно тот маршрут, который позволяет минимизировать время на дорогу. В плоском (то есть лишенном гравитации) пространстве самый быстрый путь — прямая, как вы, вероятно, и сами догадываетесь, но если пространство искривлено гравитацией, все сильно осложняется.

Представим себе, что вы бросаете мячик приятельнице. Мячик хочет долететь до нее как можно быстрее, так что, вероятно, кратчайший путь — это прямая. Но постойте! Гравитация, как мы видели в предыдущей главе, заставляет время у поверхности Земли идти самую чуточку медленнее, поэтому мяч, вероятно, доберется до вашей приятельницы быстрее, если чуточку поднимется от земли и опи-
Вселенная: руководство по эксплуатации

шет дугу. С другой стороны, если дуга окажется слишком крутой, мячу придется двигаться быстрее, а мы уже видели, что если мяч летит очень быстро, время для него замедляется. Начинается поиск компромиссов, и мяч следует кривой пространства-времени и летит по дуге. Понятно? Несмотря на все разговоры о релятивистском времени и искривленном пространстве, в слабых гравитационных полях вроде поля Земли гравитация ведет себя именно так, как предсказывал Ньютон.

Но если мы хотим разобраться, как развивается Вселенная в целом, придется вырваться из слабого поля Земли, а для этого нужно сказать два слова о метрике. Напомним, что метрика говорит нам, насколько далеко отстоят друг от друга две точки. Представьте себе» что у вас есть линейка, которая медленно сжимается. И если вы через некоторое время решите измерить, например, расстояние от вас до Парижа, то обнаружите, что оно постоянно увеличивается.

Именно это и происходит в настоящей Вселенной!

Забудьте, чему вас учили в школе: пространство не абсолютно. Мы уже видели, что пространство и время для движущихся наблюдателей и наблюдателей, которые находятся вблизи массивных тел, относительны. Теперь мы понимаем, что по мере старения Вселенной меняется само пространство.

А что же находится по правую сторону эйнштейновского уравнения поля? Уилер нам уже ответил: «Материя диктует пространству, как искривляться». Именно материя Вселенной и говорит Вселенной, как развиваться.

Как же мы разберемся во всем этом, если (на самом деле) даже не знакомы с уравнениями общей теории относительности? Не бойтесь. Помните, что, когда речь заходит о гравитации, физическая интуиция и здравый смысл помогают даже лучше, чем вы думали.

Мы тут довольно бойко рассуждали о расширении пространства, но так ничего и ве сказали о том, что же такое это самое пространство. Исаак Ньютон в своих Рrincipia Mathematica много говорил о пространстве и придумал небольшой мысленный эксперимент, позволяющий пояснить, что это такое, на конкретном примере. Вернемся далеко назад — в главу 1, где Рыжий, Галилей и Эйнштейн (не обязательно в этом порядке) обнаружили, что наблюдатель не может определить, двигается он или покоится, если движение происходит равномерно. Играет роль исключительно динамика двух наблюдателей при их относительном движении.

Ньютон представил себе, что на скрученной веревке висит ведро, полное воды. Ведро удерживают в неподвижности, а затем отпускают, и веревка начинает раскручиваться, и ведро вертится. Поначалу вода хочет остаться на месте, и стенки ведра вертятся вокруг нее. Затем вступает в действие сила трения между водой и ведром, и вода начинает крутиться вместе с ведром. И при этом взбирается вверх по стенкам.

Да, понимаем — вы читаете и думаете: «Ну и что?»

Мы так много об этом разглагольствуем, поскольку к концу эксперимента Ньютона относительное движение между ведром и водой отсутствует, — тем не менее мы можем сказать, что ведро и вода вертятся. Вот в чем вопрос: откуда ведро «знает», что оно вертится? Почему вода по-прежнему взбирается вверх по стенкам, если она никуда не движется относительно ведра?

Представьте себе одну простую вещь, которую вы увидите в любом научном музее: маятник Фуко. Маятник — это грузик, закрепленный на струне или тросе, который болтается туда-сюда, как в напольных или настенных механических часах. Маятник Фуко подвешивают так, чтобы он качался в любом направлении, куда захочет. Грузик раскачивают в одной плоскости — туда-сюда,— однако если наблюдать за ним достаточно долго, станет заметно, что он еще и вращается. То есть на самом деле маятник раскачивается в одной плоскости, а Земля под ним вращается. Каким-то образом маятник знает, как сохранить свою фиксированную ориентацию относительно пространства.

А лучше представьте себе, что наш старинный Приятель Рыжий сидит в большой цилиндрической комнате, оборудованной ракетными двигателями,— нечто вроде аттракциона-центрифуги в парке развлечений.
Вселенная: руководство по эксплуатации

Двигатели заводятся, барабан центрифуги начинает вращаться. Проходит совсем немного времени, и они останавливаются, но устройство в целом продолжает вращаться. Если вы видели «Космическую 0диссею-2001» или любой другой научно-фантастический фильм, где силу тяжести на космической станции симулируют вращением, то знаете, что произойдет: Рыжего начнет тащить вверх по стенкам[89].

Если во Вселенной нет ничего, кроме Рыжего с его центрифугой, у нас возникает вопрос: как можно сказать, что они вращаются? Относительно чего они вращаются? Попробуйте ответить на этот вопрос, избежав слова «пространство». Ведь пространство — это всего-навсего ничто, пустота, не так ли?

Философ Эрнст Мах примерно 240 лет спустя так сказал об этом в своей «Механике»: «Исследователь должен ощущать нужду в… знании о непосредственных связях, скажем, между массами во Вселенной. Они будут парить перед ним как идеальное представление о принципах материи в целом, из которого таким же образом вытекают все ускоренные и инерционные движения».

Нельзя сказать, чтобы это было точное научное определение того, как устроена Вселенная, но относительно вероятно, что мы забыли бы, что хотел донести до нас Мах, если бы не тот факт, что «принцип Маха» крайне занимал Эйнштейна (именно Эйнштейн так его и назвал). Он перефразировал это высказывание гораздо лаконичнее: «Инерция — это своего рода результат взаимодействия между телами».

По-прежнему сложно? А если так: «Тамошняя масса влияет на здешнюю инерцию»?

Ну и что? Конечно, далекая материя влияет на движение тел поблизости от нас. Именно это мы называем гравитацией. Но Мах говорил не об этом, и Эйнштейн усмотрел в его словах не это. Мах говорил, что если мы сравним нашу материю с далекими звездами, то уж как-нибудь сообразим, движемся мы или нет — по крайней мере ускоряемся мы или нет.

Принцип Маха в основном и вдохновил Эйнштейна на создание общей теории относительности. Основная идея заключалась в том, что «далекие звезды» в среднем можно считать неподвижными, и мы вправе сказать, что что-то движется или, если уж на то пошло, вращается, только относительно неподвижных звезд.

Верен ли принцип Маха?

Не обязательно. С математической точки зрения это решение уравнений Эйнштейна для пустого пространства. То есть для пространства, где материя как таковая отсутствует. Очевидно, что в таком случае не может быть и речи ни о каких далеких звездах, однако эйнштейновская специальная теория относительности все равно предсказывает, что если вы вдруг окажетесь в этой пустой вселенной, то «почувствуете», что вращаетесь.

Но ведь абсолютно пустая вселенная — это не правило, а исключение. В нашей Вселенной есть вещество. Общая теория относительности инкорпорирует во Вселенную материю. Это и есть то «свертывание» пространства, которое ощущается где угодно, в том числе и здесь.

Сразу после того, как Эйнштейн выдвинул общую теорию относительности, Джозеф Лензе и ХансТирринг из Венского университета заметили, что если взять достаточно массивное тело, скажем, черную дыру, и привести это тело во вращение, то пространство вокруг черной дырытоже потянется за ней. Иначе говоря, если вы попытаетесь стоять на месте, покажется, будто вы вращаетесь. И это не просто догадка. С тех пор было запущено множество спутников, которые зарегистрировали вращение пространства, вызванное вращением Земли и Марса.

Мы хотим сказать, что на крупных масштабах получается, будто именно материя и «создает» пространство, даже если локальное пространство выглядит так, будто в нем ничего и нет.