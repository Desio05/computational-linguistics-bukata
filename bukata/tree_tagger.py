from lxml import etree
from treetaggerwrapper import TreeTagger
import codecs

tagger = TreeTagger(TAGLANG='ru')

xml_parser = etree.XMLParser(ns_clean=True, remove_blank_text=True)
tree = etree.parse("resources/space.xml", parser=xml_parser)
with codecs.open("resources/space_tagged.out", 'w', "utf_8_sig") as output:
    for sentence in tree.getroot():
        tagged_sentence = tagger.tag_text(sentence.text)
        tagged_sentence_string = ""
        for tagged_word in tagged_sentence:
            tagged_word_split = tagged_word.split('\t')
            tagged_sentence_string += tagged_word_split[0] + "\\" + tagged_word_split[1] + " "
        output.write(tagged_sentence_string + "\n")
