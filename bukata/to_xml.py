# class work 1

import codecs
import os
import re

from lxml import etree


def write_to_xml(pathes_from, path_to):
    page = etree.Element('body')
    index = 0
    for path_from in pathes_from:
        text = codecs.open(path_from, "r", "utf_8_sig").read()
        for sentence in text.split(". "):
            value = re.sub(r'[$«»–()\.,%/—\-\"\'’…:;#№!?\r\n]', " ", sentence)
            if len(value) > 0:
                title = etree.SubElement(page, 'sentence')
                title.text = value
                title.attrib["id"] = str(index)
                index += 1
    with codecs.open(path_to, 'w', "utf_8_sig") as output:
        output.write(etree.tounicode(page, pretty_print=True))


os.chdir("resources/philosophy")
write_to_xml(os.listdir("."), "../philosophy.xml")

os.chdir("../space")
write_to_xml(os.listdir("."), "../space.xml")

os.chdir("../..")
