# class work 2 and homework 2

import os
import pickle

import pymorphy2
from lxml import etree
from nltk.corpus import stopwords as stpwrds

stopwords = stpwrds.words('russian') + ['хотя', 'который', 'ещё']


def build_index_for_xml(path_to_xml):
    inverted_index_path = path_to_xml.split(".")[0] + ".inverted_index"
    if os.path.isfile(inverted_index_path):
        return pickle.load(open(inverted_index_path, "rb"))
    xml_parser = etree.XMLParser(ns_clean=True, remove_blank_text=True)
    tree = etree.parse(path_to_xml, parser=xml_parser)
    morph = pymorphy2.MorphAnalyzer()
    inverted_index = {}
    for sentence in tree.getroot():
        normal_words = [morph.parse(word)[0].normal_form for word in sentence.text.split(' ')]
        words = [candidate for candidate in normal_words if candidate not in stopwords and not candidate == ""]
        for word in words:
            if word in inverted_index:
                inverted_index[word].append(int(sentence.attrib["id"]))
            else:
                inverted_index[word] = [int(sentence.attrib["id"])]
    with open(inverted_index_path, "wb") as output:
        pickle.dump(inverted_index, output)
    return inverted_index


def build_index_with_freq_for_xml(path_to_xml):
    inverted_index_path = path_to_xml.split(".")[0] + ".inverted_index_with_freq"
    if os.path.isfile(inverted_index_path):
        return pickle.load(open(inverted_index_path, "rb"))
    xml_parser = etree.XMLParser(ns_clean=True, remove_blank_text=True)
    tree = etree.parse(path_to_xml, parser=xml_parser)
    morph = pymorphy2.MorphAnalyzer()
    inverted_index = {}
    for sentence in tree.getroot():
        normal_words = [morph.parse(word)[0].normal_form for word in sentence.text.split(' ')]
        words = [candidate for candidate in normal_words if candidate not in stopwords and not candidate == ""]
        for word in words:
            if word in inverted_index:
                inverted_index[word][int(sentence.attrib["id"])] = words.count(word)
            else:
                inverted_index[word] = {int(sentence.attrib["id"]): words.count(word)}
    with open(inverted_index_path, "wb") as output:
        pickle.dump(inverted_index, output)
    return inverted_index


def search_with_index(index, search_list):
    morph = pymorphy2.MorphAnalyzer()
    normalized_search_list = [morph.parse(word)[0].normal_form for word in search_list]
    if set(normalized_search_list).intersection(set(stopwords)):
        raise ValueError("Stopwords cannot be found")
    result = set(index[normalized_search_list[0]] if normalized_search_list[0] in index else [])
    for to_search in normalized_search_list:
        if to_search not in index:
            return set([])
        result.intersection(index[to_search])
    return result


def print_sentences(xml_path, sentences_ids):
    xml_parser = etree.XMLParser(ns_clean=True, remove_blank_text=True)
    tree = etree.parse(xml_path, parser=xml_parser)
    for sentence in tree.getroot():
        if int(sentence.attrib["id"]) in sentences_ids:
            print(sentence.text)
            print()


if __name__ == '__main__':
    space_inverted_index = build_index_for_xml("resources/space.xml")
    philosophy_inverted_index = build_index_for_xml("resources/philosophy.xml")

    space_sentence_indexes = search_with_index(space_inverted_index, ["черная", "дыра"])
    philosophy_sentence_indexes = search_with_index(philosophy_inverted_index, ["философском", "материализме"])

    print()
    print("Search results \"черная дыра\" in space.xml...")
    print_sentences("resources/space.xml", space_sentence_indexes)
    print()
    print("Search results \"философском материализме\" in philosophy.xml...")
    print_sentences("resources/philosophy.xml", philosophy_sentence_indexes)
